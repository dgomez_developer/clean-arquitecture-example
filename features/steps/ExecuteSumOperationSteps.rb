require 'calabash-android/calabash_steps'

Given(/^The application main screen$/) do

end

When(/^the user touches the "(.*?)" field$/) do |arg1|
  touch("edittext id:'#{arg1}'")
end


When(/^the user types "(.*?)"$/) do |arg1|
  keyboard_enter_text('#{arg1}')
end

Then(/^the user touches button "(.*?)"$/) do |arg1|
  touch("button id:'#{arg1}'")
end

Then(/^the user should see the result "(.*?)"$/) do |arg1|
  if(!query("textview",'#{arg1}'))
    fail("It is not working")
  end
end