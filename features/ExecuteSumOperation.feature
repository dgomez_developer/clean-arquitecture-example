Feature:
  As a user of the application
  I want to sum two numbers
  so that I can see the result on the screen

  Scenario: Execute sum operation
    Given The application main screen
    When the user touches the "activity_main_sum_first_number_edittext" field
    When the user types "2"
    When the user touches the "activity_main_sum_second_number_edittext" field
    When the user types "3"
    Then the user touches button "activity_main_sum_button"
    Then the user should see the result "5"
