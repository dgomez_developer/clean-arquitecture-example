package es.babel.deboragomez.myapplication;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.TextView;

import org.hamcrest.core.IsEqual;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import es.babel.deboragomez.myapplication.android.MainActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.assertThat;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Tests the functionality in the Main activity for the sum operation.
 */
@RunWith(AndroidJUnit4.class)
public class ExpressoMainActivity {

    @Rule
    public ActivityTestRule<MainActivity> mMainActivityRule = new ActivityTestRule<>(
            MainActivity.class);

    @Test
    public void testSuccessfulSumOperation() {
        enterFirstDigit(3);
        enterSecondDigit(4);
        clickOnComputeSum();
        TextView resultTextView = (TextView) mMainActivityRule.getActivity().findViewById(R.id.activity_main_sum_operation_result_textview);
        String result = resultTextView.getText().toString();
        assertThat("Result is equal", "7", org.hamcrest.Matchers.is(IsEqual.equalTo(result)));
    }

    private void clickOnComputeSum() {
        onView(withId(R.id.activity_main_sum_button)).perform(click());
    }

    private void enterSecondDigit(int number) {
        onView(withId(R.id.activity_main_sum_second_number_edittext)).perform(clearText());
        onView(withId(R.id.activity_main_sum_second_number_edittext)).perform(typeText(String.valueOf(number)));
    }

    private void enterFirstDigit(int number) {
        onView(withId(R.id.activity_main_sum_first_number_edittext)).perform(clearText());
        onView(withId(R.id.activity_main_sum_first_number_edittext)).perform(typeText(String.valueOf(number)));
    }


}
