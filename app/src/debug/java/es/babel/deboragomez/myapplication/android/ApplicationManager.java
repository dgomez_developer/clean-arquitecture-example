package es.babel.deboragomez.myapplication.android;

import android.app.Application;

import es.babel.deboragomez.myapplication.dagger.components.ApplicationComponent;
import es.babel.deboragomez.myapplication.dagger.components.DaggerApplicationComponent;
import es.babel.deboragomez.myapplication.dagger.modules.ApplicationModule;

/**
 * Debug Android Application component
 */
public class ApplicationManager extends Application {


    private ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initInjection();
    }

    /**
     * Get Application dagger injector
     *
     * @return ApplicationComponent injector instance
     */
    public ApplicationComponent getApplicationComponent() {
        return mApplicationComponent;
    }

    private void initInjection() {
        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(getApplicationContext())).build();
    }
}
