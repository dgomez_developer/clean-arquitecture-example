package es.babel.deboragomez.myapplication;

import java.util.concurrent.Executor;

/**
 * Class that implements the Executor interface to execute the unit tests in the same thread to be able to check results synchronously
 */
public class SameThreadExecutor implements Executor {

    @Override
    public void execute(Runnable command) {
        command.run();
    }
}
