package es.babel.deboragomez.myapplication.core.interactors;

import es.babel.deboragomez.myapplication.core.entities.GenericCallback;

/**
 * Interface to be implemented by the button use cases
 */
public interface SumInteractor {

    /**
     * Use case execution operation
     *
     * @param callback use case observer
     * @param number1  first number to sum
     * @param number2  second number to sum
     */
    void execute(int number1, int number2, GenericCallback<Integer> callback);
}
