package es.babel.deboragomez.myapplication.core.gateways;

import es.babel.deboragomez.myapplication.core.entities.GenericCallback;

/**
 * Gateway interface implemented by the operations repository
 */
public interface OperationsRepositoryGateway {

    /**
     * Method that stores the result of an operation in the repository
     *
     * @param result   Operation result
     * @param callback called when the result has been stored
     */
    void storeResult(int result, GenericCallback<Void> callback);

}
