package es.babel.deboragomez.myapplication.core.exceptions;

/**
 * Types of exceptions launched by the app
 */
public enum CoreExceptionType {

    DATABASE_INTERNAL_ERROR, GENERIC
}
