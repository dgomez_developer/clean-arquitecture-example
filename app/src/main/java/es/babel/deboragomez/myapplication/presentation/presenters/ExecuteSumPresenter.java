package es.babel.deboragomez.myapplication.presentation.presenters;

import java.util.concurrent.Executor;

import javax.inject.Inject;

import es.babel.deboragomez.myapplication.android.exceptions.UiError;
import es.babel.deboragomez.myapplication.core.entities.GenericCallback;
import es.babel.deboragomez.myapplication.core.exceptions.CoreException;
import es.babel.deboragomez.myapplication.core.interactors.SumInteractor;

/**
 * Presenter for executing a sum
 */
public class ExecuteSumPresenter {


    private final Executor mExecutor;
    private final SumInteractor mInteractor;

    @Inject
    public ExecuteSumPresenter(Executor executor, SumInteractor interactor) {
        this.mExecutor = executor;
        this.mInteractor = interactor;
    }

    /**
     * Calculates the sum of two numbers
     *
     * @param callback use case observer
     * @param number1  first number to sum
     * @param number2  second number to sum
     */
    public void calculateSum(final int number1, final int number2, final SumListener callback) {

        mExecutor.execute(new Runnable() {
            @Override
            public void run() {
                mInteractor.execute(number1, number2, new GenericCallback<Integer>() {
                    @Override
                    public void onSuccess(Integer response) {
                        callback.onSumExecuted(response);
                    }

                    @Override
                    public void onFailure(CoreException e) {
                        callback.onSumError(new UiError(e));
                    }
                });
            }
        });


    }

    /**
     * Listener for the Ui of the sum operation
     */
    public interface SumListener {

        /**
         * Called when the sum operation finishes
         *
         * @param result result of the operation
         */
        void onSumExecuted(int result);

        /**
         * Called when an error happens when executing the opration
         *
         * @param error error containing a UI message for the user
         */
        void onSumError(UiError error);
    }
}
