package es.babel.deboragomez.myapplication.dagger.modules;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Provider of the main dependencies for the Android application, this class will provide objects
 * that live during the application lifecycle, like de application context.
 */
@Module
public class ApplicationModule {

    private final Context mApplicationContext;

    public ApplicationModule(Context context) {
        this.mApplicationContext = context;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return mApplicationContext;
    }

}
