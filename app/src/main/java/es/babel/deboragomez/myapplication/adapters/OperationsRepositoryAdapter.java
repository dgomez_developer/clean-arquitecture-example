package es.babel.deboragomez.myapplication.adapters;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import es.babel.deboragomez.myapplication.core.entities.GenericCallback;
import es.babel.deboragomez.myapplication.core.exceptions.CoreException;
import es.babel.deboragomez.myapplication.core.exceptions.CoreExceptionType;
import es.babel.deboragomez.myapplication.core.gateways.OperationsRepositoryGateway;

/**
 * Class that stores the result into a List entity
 */
public class OperationsRepositoryAdapter implements OperationsRepositoryGateway {

    private final List<Integer> mResults;

    @Inject
    public OperationsRepositoryAdapter() {
        this.mResults = new ArrayList<>();
    }

    @Override
    public void storeResult(int result, GenericCallback<Void> callback) {
        if (mResults == null) {
            callback.onFailure(new CoreException(CoreExceptionType.DATABASE_INTERNAL_ERROR));
            return;
        }
        mResults.add(result);
        callback.onSuccess(null);
    }

    public List<Integer> getResults() {
        return mResults;
    }
}
