package es.babel.deboragomez.myapplication.android.exceptions;

import es.babel.deboragomez.myapplication.core.exceptions.CoreException;

/**
 * This class parses the app exceptions into a readable UI message for the user
 */
public class UiError {

    private int mErrorMessageResourceId;
    private final CoreException mCoreException;

    /**
     * Constructor that receives a Core exception and parses the exception type into a String
     * resource Id
     *
     * @param exception Core Exception
     */
    public UiError(CoreException exception) {
        this.mErrorMessageResourceId = UiErrorMessages.getMessageId(exception.getType());
        this.mCoreException = exception;
    }

    public int getErrorMessageId() {
        return mErrorMessageResourceId;
    }

    public CoreException getCoreException() {
        return this.mCoreException;
    }

    public void setErrorMessageId(int errorMessage) {
        this.mErrorMessageResourceId = errorMessage;
    }

}
