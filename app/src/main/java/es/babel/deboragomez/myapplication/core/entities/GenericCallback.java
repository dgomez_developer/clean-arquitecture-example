package es.babel.deboragomez.myapplication.core.entities;

import es.babel.deboragomez.myapplication.core.exceptions.CoreException;

/**
 * Generic listener interface to be used by the whole application
 */
public interface GenericCallback<T> {

    /**
     * Called when the operation successes
     *
     * @param response response object
     */
    void onSuccess(T response);

    /**
     * Called when the operation fails
     *
     * @param e exception object
     */
    void onFailure(CoreException e);
}
