package es.babel.deboragomez.myapplication.core.exceptions;

/**
 * Exceptions launch by the whole app
 */
public class CoreException extends Exception {

    private final CoreExceptionType mType;

    /**
     * Constructor that receives a type of exception
     *
     * @param type Exception type
     */
    public CoreException(CoreExceptionType type) {
        this.mType = type;
    }

    /**
     * Constructor that receives a type of exception and the original exception launched
     *
     * @param type Type of exception
     * @param exception    Original Exception
     */
    public CoreException(CoreExceptionType type, Exception exception) {
        super(exception);
        this.mType = type;
    }

    public CoreExceptionType getType() {
        return mType;
    }


}
