package es.babel.deboragomez.myapplication.core.interactors;

import javax.inject.Inject;

import es.babel.deboragomez.myapplication.core.entities.GenericCallback;
import es.babel.deboragomez.myapplication.core.exceptions.CoreException;
import es.babel.deboragomez.myapplication.core.gateways.OperationsRepositoryGateway;

/**
 * Class that implements the use case for the sum operation
 */
public class SumUseCase implements SumInteractor {

    private OperationsRepositoryGateway mRepository;

    @Inject
    public SumUseCase(OperationsRepositoryGateway gateway) {
        this.mRepository = gateway;
    }


    @Override
    public void execute(final int number1, final int number2, final GenericCallback<Integer> callback) {
        mRepository.storeResult(number1 + number2, new GenericCallback<Void>() {
            @Override
            public void onSuccess(Void response) {
                callback.onSuccess(number1 + number2);
            }

            @Override
            public void onFailure(CoreException e) {
                callback.onFailure(e);
            }
        });
    }
}
