package es.babel.deboragomez.myapplication.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import es.babel.deboragomez.myapplication.R;
import es.babel.deboragomez.myapplication.android.exceptions.UiError;
import es.babel.deboragomez.myapplication.presentation.presenters.ExecuteSumPresenter;

public class MainActivity extends Activity implements ExecuteSumPresenter.SumListener {

    private TextView mResultTextView;
    private ExecuteSumPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mResultTextView = (TextView) findViewById(R.id.activity_main_sum_operation_result_textview);
        final EditText mFirstNumberEditText = (EditText) findViewById(R.id.activity_main_sum_first_number_edittext);
        final EditText mSecondNumberEditText = (EditText) findViewById(R.id.activity_main_sum_second_number_edittext);
        final Button mCalculateButton = (Button) findViewById(R.id.activity_main_sum_button);
        mPresenter = ((ApplicationManager) getApplicationContext()).getApplicationComponent().provideSumPresenter();
        mCalculateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.calculateSum(Integer.parseInt(mFirstNumberEditText.getText().toString()), Integer.parseInt(mSecondNumberEditText.getText().toString()), MainActivity.this);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSumExecuted(final int result) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mResultTextView.setText(String.valueOf(result));
            }
        });
    }

    @Override
    public void onSumError(final UiError error) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog dialog = new AlertDialog.Builder(MainActivity.this).setMessage(error.getErrorMessageId()).create();
                dialog.show();
            }
        });
    }
}
