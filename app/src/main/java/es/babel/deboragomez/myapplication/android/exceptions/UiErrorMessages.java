package es.babel.deboragomez.myapplication.android.exceptions;

import java.util.HashMap;
import java.util.Map;

import es.babel.deboragomez.myapplication.R;
import es.babel.deboragomez.myapplication.core.exceptions.CoreExceptionType;

/**
 * This class maps all the error types into corresponding String messages
 */
public final class UiErrorMessages {


    private static final Map<CoreExceptionType, Integer> ERROR_MAP = createErrorMap();

    private UiErrorMessages() {

    }

    private static Map<CoreExceptionType, Integer> createErrorMap() {
        Map<CoreExceptionType, Integer> errorMap = new HashMap<CoreExceptionType, Integer>();
        errorMap.put(CoreExceptionType.GENERIC,
                R.string.Application_Generic_Error);
        return errorMap;
    }

    /**
     * Method that parses the exception type into a readable message for the user
     *
     * @param type Type of Core exception
     * @return the String resource id
     */
    public static int getMessageId(CoreExceptionType type) {
        if (ERROR_MAP.containsKey(type)) {
            return ERROR_MAP.get(type);
        } else {
            return R.string.Application_Generic_Error;
        }
    }


}
