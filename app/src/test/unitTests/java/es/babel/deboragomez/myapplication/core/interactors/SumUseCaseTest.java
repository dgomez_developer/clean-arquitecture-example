package es.babel.deboragomez.myapplication.core.interactors;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import es.babel.deboragomez.myapplication.core.entities.GenericCallback;
import es.babel.deboragomez.myapplication.core.gateways.OperationsRepositoryGateway;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * Unit tests for class SumUseCase
 */
public class SumUseCaseTest {

    private SumUseCase mUseCaseUnderTest;
    private OperationsRepositoryGateway mMockRepositoryGateway;
    private GenericCallback<Integer> mSpyObserver;

    @Before
    public void setup() {
        mSpyObserver = mock(GenericCallback.class);
        mMockRepositoryGateway = mock(OperationsRepositoryGateway.class);
        mUseCaseUnderTest = new SumUseCase(mMockRepositoryGateway);
    }

    @Test
    public void whenGivenTwoNumbersAndOperationSuccessThenCallbackIsCalledWithTheResult() {
        mockGatewayOperation();
        mUseCaseUnderTest.execute(2, 3, mSpyObserver);
        ArgumentCaptor<Integer> captor = ArgumentCaptor.forClass(Integer.class);
        verify(mSpyObserver).onSuccess(captor.capture());
        verifyNoMoreInteractions(mSpyObserver);
        assertEquals(5, captor.getAllValues().get(0), 0);
    }

    private void mockGatewayOperation() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                GenericCallback<Void> callback = (GenericCallback<Void>) invocation.getArguments()[1];
                callback.onSuccess(null);
                return null;
            }
        }).when(mMockRepositoryGateway).storeResult(anyInt(), any(GenericCallback.class));
    }
}