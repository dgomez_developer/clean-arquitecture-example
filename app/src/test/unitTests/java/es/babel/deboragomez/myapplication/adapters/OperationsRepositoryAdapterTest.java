package es.babel.deboragomez.myapplication.adapters;

import org.junit.Before;
import org.junit.Test;

import es.babel.deboragomez.myapplication.core.entities.GenericCallback;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * Unit tests for class OperationsRepositoryAdapter
 */
public class OperationsRepositoryAdapterTest {


    private OperationsRepositoryAdapter mAdapterUnderTest;
    private GenericCallback<Void> mSpyObserver;

    @Before
    public void setup() {
        mSpyObserver = mock(GenericCallback.class);
        mAdapterUnderTest = new OperationsRepositoryAdapter();
    }

    @Test
    public void whenAResultIsGivenThenTheSameResultIsStored() {
        mAdapterUnderTest.storeResult(5, mSpyObserver);
        verify(mSpyObserver).onSuccess(null);
        verifyNoMoreInteractions(mSpyObserver);
        assertFalse(mAdapterUnderTest.getResults().isEmpty());
        assertEquals(5, mAdapterUnderTest.getResults().get(0), 0);
    }

}
