package es.babel.deboragomez.myapplication.presentation.presenters;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import es.babel.deboragomez.myapplication.SameThreadExecutor;
import es.babel.deboragomez.myapplication.core.entities.GenericCallback;
import es.babel.deboragomez.myapplication.core.interactors.SumInteractor;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * Unit tests for class ExecuteSumPresenter
 */
public class ExecuteSumPresenterTest {

    private ExecuteSumPresenter mPresenterUnderTest;
    private SumInteractor mMockInteractor;
    private ExecuteSumPresenter.SumListener mSpyListener;

    @Before
    public void setup() {
        mSpyListener = mock(ExecuteSumPresenter.SumListener.class);
        mMockInteractor = mock(SumInteractor.class);
        mPresenterUnderTest = new ExecuteSumPresenter(new SameThreadExecutor(), mMockInteractor);
    }

    @Test
    public void whenExecutingTheOperationThanOperationSucceed() {
        mockInteractorExecution();
        mPresenterUnderTest.calculateSum(2, 3, mSpyListener);
        ArgumentCaptor<Integer> captor = ArgumentCaptor.forClass(Integer.class);
        verify(mSpyListener).onSumExecuted(captor.capture());
        verifyNoMoreInteractions(mSpyListener);
        assertEquals(5, captor.getAllValues().get(0), 0);
    }

    private void mockInteractorExecution() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                GenericCallback<Integer> callback = (GenericCallback<Integer>) invocation.getArguments()[2];
                callback.onSuccess(5);
                return null;
            }
        }).when(mMockInteractor).execute(anyInt(),anyInt(),any(GenericCallback.class));
    }

}
