package es.babel.deboragomez.myapplication.jbehave;

import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.embedder.Embedder;
import org.jbehave.core.embedder.StoryControls;
import org.jbehave.core.io.LoadFromRelativeFile;
import org.jbehave.core.junit.JUnitStory;
import org.jbehave.core.reporters.CrossReference;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.junit.Ignore;

import java.io.File;
import java.net.MalformedURLException;
import java.util.Properties;

import static java.util.Arrays.asList;

/**
 * JBehave configuration
 */
@Ignore
public class BaseStoryConfiguration extends JUnitStory {
    private final CrossReference xref = new CrossReference();

    public BaseStoryConfiguration() {
        Embedder embedder = configuredEmbedder();
        embedder.embedderControls().doGenerateViewAfterStories(true).doIgnoreFailureInStories(false)
                .doIgnoreFailureInView(true).useThreads(1).useStoryTimeouts("2000");
        embedder.useMetaFilters(asList("-failing true"));
    }

    @Override
    public Configuration configuration() {
        String urlFile = "src/test/integrationTests/java";
        Properties properties = new Properties();
        properties.setProperty("encoding", "UTF-8");
        try {
            return new MostUsefulConfiguration().useStoryControls(new StoryControls())
                    .useStoryLoader(new LoadFromRelativeFile(new File(urlFile).toURI().toURL()))
                    .useStoryReporterBuilder(new StoryReporterBuilder().withDefaultFormats()
                            .withFormats(org.jbehave.core.reporters.Format.CONSOLE, org.jbehave.core.reporters.Format.XML, org.jbehave.core.reporters.Format.HTML).withFailureTrace(true)
                            .withFailureTraceCompression(true).withCrossReference(xref));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }

}
