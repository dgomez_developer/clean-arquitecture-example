package es.babel.deboragomez.myapplication.jbehave.steps;

import org.jbehave.core.annotations.Alias;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.mockito.ArgumentCaptor;

import javax.inject.Named;

import es.babel.deboragomez.myapplication.SameThreadExecutor;
import es.babel.deboragomez.myapplication.adapters.OperationsRepositoryAdapter;
import es.babel.deboragomez.myapplication.core.interactors.SumUseCase;
import es.babel.deboragomez.myapplication.presentation.presenters.ExecuteSumPresenter;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * Steps for sum operation story
 */
public class ExecuteSumOperationSteps {

    private ExecuteSumPresenter mPresenter;
    private ExecuteSumPresenter.SumListener mResultObserver;

    @Given("the application")
    public void prepareEnvironment() {
        mResultObserver = mock(ExecuteSumPresenter.SumListener.class);
        mPresenter = new ExecuteSumPresenter(new SameThreadExecutor(), new SumUseCase(new OperationsRepositoryAdapter()));
    }

    @When("the user introduces the numbers <first_number> and <second_number>")
    @Alias("the user introduces the numbers $first_number and $second_number")
    public void executeSumOperation(@Named("first_number") int number1, @Named("second_number") int number2) {
        mPresenter.calculateSum(number1, number2, mResultObserver);
    }

    @Then("the result is <result>")
    @Alias("the result is $result")
    public void checkResult(@Named("result") int result) {
        ArgumentCaptor<Integer> captor = ArgumentCaptor.forClass(Integer.class);
        verify(mResultObserver).onSumExecuted(captor.capture());
        verifyNoMoreInteractions(mResultObserver);
        assertEquals(result, captor.getAllValues().get(0), 0);

    }

}
