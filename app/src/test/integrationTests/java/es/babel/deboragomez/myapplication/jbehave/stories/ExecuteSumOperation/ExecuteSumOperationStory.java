package es.babel.deboragomez.myapplication.jbehave.stories.ExecuteSumOperation;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;
import es.babel.deboragomez.myapplication.jbehave.BaseStoryConfiguration;
import es.babel.deboragomez.myapplication.jbehave.steps.ExecuteSumOperationSteps;

/**
 * Configuration for execute sum operation story
 */
@RunWith(JUnitReportingRunner.class)
public class ExecuteSumOperationStory extends BaseStoryConfiguration {

    @Override
    public InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(), new ExecuteSumOperationSteps());
    }
}
