Narrative:

As a user of the application
I want to sum two numbers
so that I can see the result on the screen



Scenario: Execute sum operation
Meta:
@TestCode Execute sum operation

Given the application
When the user introduces the numbers <first_number> and <second_number>
Then the result is <result>

Examples:
|first_number|second_number|result|
|2|3|5|
|-1|2|1|