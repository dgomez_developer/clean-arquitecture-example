package es.babel.deboragomez.myapplication.dagger.components;

import javax.inject.Singleton;

import dagger.Component;
import es.babel.deboragomez.myapplication.dagger.modules.ApplicationModule;
import es.babel.deboragomez.myapplication.dagger.modules.SumExecutionModule;
import es.babel.deboragomez.myapplication.presentation.presenters.ExecuteSumPresenter;

/**
 * Release Application injector
 */
@Singleton
@Component(modules = {ApplicationModule.class, SumExecutionModule.class})
public interface ApplicationComponent {

    /**
     * Exposes an instance of the Sum Presenter
     *
     * @return an instance
     */
    ExecuteSumPresenter provideSumPresenter();

}
