package es.babel.deboragomez.myapplication.dagger.modules;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import es.babel.deboragomez.myapplication.adapters.OperationsRepositoryAdapter;
import es.babel.deboragomez.myapplication.core.gateways.OperationsRepositoryGateway;
import es.babel.deboragomez.myapplication.core.interactors.SumInteractor;
import es.babel.deboragomez.myapplication.core.interactors.SumUseCase;

/**
 * This module defines the instances of the components involved in the execution of the sum operation
 */
@Module
public class SumExecutionModule {

    /**
     * Provides de created SumUseCase instance when injecting a SumInteractor
     *
     * @param useCase SumUseCase instance
     * @return an instance of SumUseCase
     */
    @Provides
    @Singleton
    SumInteractor provideSumUseCase(SumUseCase useCase) {
        return useCase;
    }

    /**
     * Provides de created OperationsRepositoryAdapter instance when injecting an OperationsRepositoryGateway
     *
     * @param adapter OperationsRepositoryAdapter instance
     * @return an instance of SumUseCase
     */
    @Provides
    @Singleton
    OperationsRepositoryGateway provideOperationRepositoryAdapter(OperationsRepositoryAdapter adapter) {
        return adapter;
    }

    /**
     * Provides de created newSingleThreadExecutor instance when injecting an Executor
     *
     * @return an instance of newSingleThreadExecutor
     */
    @Provides
    Executor provideExecutor() {
        return Executors.newSingleThreadExecutor();
    }
}
