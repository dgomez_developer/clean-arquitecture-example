Android Clean Architecture Application.

This application has been developed under Clean architecture premises in order to establish 
a standard Android application structure to be follwed by all projects comming from CDM 
division.

To get started with clean architecture, take a look at this article:
http://fernandocejas.com/2014/09/03/architecting-android-the-clean-way/

The application uses some well-known libraries like dagger for dependencies injection,
jbehave for integration tests, expresso for UX/UI tests and mockito for unit tests.

JaCoCo plugin is used to meadure the code coverage and generate reports for presenting the 
information on Sonar Qube.

The application configuration files have been prepared to work under a Continuous Integration
environment in CDM division. The following link explains how this environment works:
http://gp.babel.es/projects/i-cdm/wiki/Control_de_versiones_Integraci%C3%B3n_cont%C3%ADnua

Feel free to ask any questions!!

